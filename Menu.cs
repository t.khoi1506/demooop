﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo
{
    public abstract class Menu
    {
        protected String Ma;
        protected String Ten;
        protected int Gia;

        public virtual void Info()
        {
            Console.WriteLine("Ma: " + Ma + " Ten: " + Ten + " Gia: " + Gia);
        }
        public Menu()
        {

        }
        public Menu(string m, string t, int g)
        {
            Ma = m;
            Ten = t;
            Gia = g;
        }

        public string ma { get => Ma; set => Ma = value; }
        public string ten { get => Ten; set => Ten = value; }
        public int gia { get => Gia; set => Gia = value; }

    }
    class MonAn : Menu
    {
        public MonAn()
        {
            Ma = "123456";
            Ten = "abc";
            Gia = 10000;
        }
        public MonAn(string m, string t, int g) : base (m, t, g)
        {

        }
        public new void Info()
        {
            Console.WriteLine("Thong tin mon an: ");
            base.Info();
        }
    }
    class ThucUong : Menu
    {
        public ThucUong()
        {
            Ma = "7891011";
            Ten = "xyz";
            Gia = 20000;
        }
        public ThucUong(string m, string t, int g) : base (m, t, g)
        {

        }
        public override void Info()
        {
            Console.WriteLine("Thông tin thuc uong: ");
            base.Info();
        }
    }
}
